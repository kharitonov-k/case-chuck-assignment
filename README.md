# CaseChuckAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.5.

![presentation](case-chuck-assignment.gif)

## About the app

The app shows jokes about Chuck Norris.

### It allows 
- to retrieve 10 random jokes via pressing the "get jokes" button
- to mark a joke as a favorite
- to add random jokes to the favorites automatically

### Limitations
- only an authorized user can have favorites
- only 10 jokes can be placed to the favorites

### Solution

- UI framework is Angular Material
- The mock stub is provided via `ApiMockInterceptor` which will be in use after `--configuration mock` indicating 
- The state management is based on the page-service approach

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
