export const environment = {
  production: true,
  api: 'https://api.icndb.com',
  maxFavorites: 10
};
