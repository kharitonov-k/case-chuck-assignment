export interface Joke {
  id: number;
  categories: string[];
  joke: string;
}

export interface JokeResponse {
  type: 'success' | 'fail',
  value: Joke[];
}

export interface JokesFavorite {
  [ id: number ]: Joke
}

export interface JokesFavoriteDB {
  [ token: string ]: JokesFavorite
}
