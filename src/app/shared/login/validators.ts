import { AbstractControl, ValidationErrors } from '@angular/forms';

export const increasingStraight = (amount: number, length: number) => (control: AbstractControl): ValidationErrors | null => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';
  const matches = alphabet.split(/\B/).filter((letter: string, index: number) => {
    if (index > alphabet.length - length) {
      return false;
    }
    const str = alphabet.slice(index, index + length);
    return control.value.toLowerCase().indexOf(str) !== -1;
  });
  if (matches.length >= amount) {
    return null;
  }
  return {
    increasingStraight: { amount, length }
  };
};

export const lowerCaseAlphabeticCharacters = (control: AbstractControl): ValidationErrors | null => {
  if (/^[a-z]*$/.test(control.value)) {
    return null;
  }
  return {
    lowerCaseAlphabeticCharacters: true
  }
};

export const mayNotContain = (symbols: string) => (control: AbstractControl): ValidationErrors | null => {
  if (new RegExp(`^[^${symbols}]*$`).test(control.value)) {
    return null;
  }
  return {
    mayNotContain: symbols.split(/\B/).join(', ')
  }
};

export const nonOverlappingPairs = (amount: number) => (control: AbstractControl): ValidationErrors | null => {
  const matches = control.value.match(/(\w)\1+/g);
  if (matches && matches.length >= amount) {
    return null;
  }
  return {
    nonOverlappingPairs: amount
  };
};
