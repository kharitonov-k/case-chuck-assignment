import { increasingStraight, lowerCaseAlphabeticCharacters, mayNotContain, nonOverlappingPairs } from './validators';
import { FormControl } from '@angular/forms';

describe('Validators', () => {
  let control: FormControl;

  beforeEach(() => {
    control = new FormControl();
  });

  it('increasingStraight', () => {
    control.setValue('abs');
    expect(increasingStraight(1, 2)(control)).toBe(null);
    control.setValue('as');
    expect(increasingStraight(1, 2)(control)).toEqual({ increasingStraight: { amount: 1, length: 2 } });
    control.setValue('zzzabc');
    expect(increasingStraight(1, 3)(control)).toBe(null);
    control.setValue('zzzbc');
    expect(increasingStraight(1, 3)(control)).toEqual({ increasingStraight: { amount: 1, length: 3 } });
    control.setValue('xyzabc');
    expect(increasingStraight(2, 3)(control)).toBe(null);
    control.setValue('xyzabc');
    expect(increasingStraight(3, 3)(control)).toEqual({ increasingStraight: { amount: 3, length: 3 } });
  });

  it('lowerCaseAlphabeticCharacters', () => {
    control.setValue('abswrewerrewr');
    expect(lowerCaseAlphabeticCharacters(control)).toBe(null);
    control.setValue('Asf');
    expect(lowerCaseAlphabeticCharacters(control)).toEqual({ lowerCaseAlphabeticCharacters: true });
    control.setValue('qas34');
    expect(lowerCaseAlphabeticCharacters(control)).toEqual({ lowerCaseAlphabeticCharacters: true });
    control.setValue('wdw(&');
    expect(lowerCaseAlphabeticCharacters(control)).toEqual({ lowerCaseAlphabeticCharacters: true });
  });

  it('mayNotContain', () => {
    control.setValue('abs');
    expect(mayNotContain('xcv')(control)).toBe(null);
    control.setValue('xertt');
    expect(mayNotContain('xcv')(control)).toEqual({ mayNotContain: 'x, c, v' });
  });

  it('nonOverlappingPairs', () => {
    control.setValue('ggwefcwfcqq');
    expect(nonOverlappingPairs(2)(control)).toBe(null);
    control.setValue('ggwefcwfcqq');
    expect(nonOverlappingPairs(3)(control)).toEqual({ nonOverlappingPairs: 3 });
  });
});
