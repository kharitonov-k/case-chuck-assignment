import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { of } from 'rxjs';
import { delay, take } from 'rxjs/operators';

import { increasingStraight, lowerCaseAlphabeticCharacters, mayNotContain, nonOverlappingPairs } from './validators';
import { AuthService, calcToken, getTokens } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ]
})
export class LoginComponent {

  readonly formSignIn = this.formBuilder.group({
    username: [ '', Validators.required ],
    password: [ '', Validators.required ]
  });

  readonly formSignUp = this.formBuilder.group({
    username: [ '', Validators.required ],
    password: [ '', [
      Validators.required,
      increasingStraight(1, 3),
      mayNotContain('iol'),
      nonOverlappingPairs(2),
      Validators.maxLength(32),
      lowerCaseAlphabeticCharacters
    ] ]
  });

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly matDialogRef: MatDialogRef<LoginComponent>,
    private readonly authService: AuthService,
    private readonly matSnackBar: MatSnackBar,
  ) {}

  getError(form: FormGroup, errorCode: string, path?: Array<string | number> | string): any {
    return form.getError(errorCode, path);
  }

  async onSingIn(): Promise<void> {
    if (this.formSignIn.invalid) {
      return;
    }
    const { username, password } = this.formSignIn.value;
    return await calcToken(username, password).then(token => {
      if (getTokens().includes(token)) {
        this.success(token);
        return;
      }
      const matSnackBarRef = this.matSnackBar.open(`Logging in is failed`);
      of(void 0).pipe(delay(1000)).subscribe(() => matSnackBarRef?.dismiss());
    })
  }

  async onSingUp(): Promise<void> {
    if (this.formSignUp.invalid) {
      return;
    }
    const { username, password } = this.formSignUp.value;
    return await calcToken(username, password).then(token => this.success(token));
  }

  private success(token: string): void {
    this.authService.isAuthorized$.pipe(take(1)).subscribe(() => {
      this.matDialogRef.close(true);
    });
    this.authService.updateToken(token);
  }
}
