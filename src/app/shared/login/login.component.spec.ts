import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { of } from 'rxjs';

import { AuthService, authTokensStoreKey } from '../auth.service';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let matSnackBar: MatSnackBar;
  let matDialogRef: MatDialogRef<LoginComponent>;
  let authService: AuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      imports: [
        ReactiveFormsModule
      ],
      providers: [
        {
          provide: MatSnackBar,
          useValue: {
            open() {}
          }
        },
        {
          provide: MatDialogRef,
          useValue: {
            close() {}
          }
        },
        {
          provide: AuthService,
          useValue: {
            // @ts-ignore
            get isAuthorized$() {},
            updateToken() {}
          }
        }
      ],
      declarations: [ LoginComponent ]
    }).compileComponents();
  });

  beforeEach(() => {
    matSnackBar = TestBed.inject(MatSnackBar);
    matDialogRef = TestBed.inject(MatDialogRef);
    authService = TestBed.inject(AuthService);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('should sign in', () => {
    it('failed', async () => {
      spyOn(localStorage, 'getItem').withArgs(authTokensStoreKey).and.returnValue(`[]`);
      const spyOnMatSnackBar = spyOn(matSnackBar, 'open');
      component.formSignIn.patchValue({ username: 'user', password: 'abccdd' });
      await component.onSingIn();
      expect(spyOnMatSnackBar).toHaveBeenCalled();
    });
    it('success', async () => {
      const token = '0649d792f34d1187be004c1797af304dd1c20586b67c9e70fdc2bf7a68537789';
      spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
      spyOn(localStorage, 'getItem').withArgs(authTokensStoreKey).and.returnValue(`["${token}"]`);
      const spyOnClose = spyOn(matDialogRef, 'close');
      const spyOnUpdateToken = spyOn(authService, 'updateToken');
      component.formSignIn.patchValue({ username: 'user', password: 'abccdd' });
      await component.onSingIn();
      expect(spyOnClose).toHaveBeenCalledWith(true);
      expect(spyOnUpdateToken).toHaveBeenCalledWith(token);
    });
  });

  describe('should sign up', () => {
    it('success', async () => {
      const token = '0649d792f34d1187be004c1797af304dd1c20586b67c9e70fdc2bf7a68537789';
      spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
      spyOn(localStorage, 'getItem').withArgs(authTokensStoreKey).and.returnValue(`[]`);
      const spyOnClose = spyOn(matDialogRef, 'close');
      const spyOnUpdateToken = spyOn(authService, 'updateToken');
      component.formSignUp.patchValue({ username: 'user', password: 'abccdd' });
      await component.onSingUp();
      expect(spyOnClose).toHaveBeenCalledWith(true);
      expect(spyOnUpdateToken).toHaveBeenCalledWith(token);
    });
  });
});
