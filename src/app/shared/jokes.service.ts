import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MatSnackBar } from '@angular/material/snack-bar';

import { Observable, of, Subject } from 'rxjs';
import { catchError, delay, pluck, shareReplay, switchMap } from 'rxjs/operators';

import { Joke, JokeResponse } from './jokes.types';

@Injectable({
  providedIn: 'root'
})
export class JokesService {

  private readonly trigger$ = new Subject<void>();

  readonly jokes$: Observable<Joke[]> = this.trigger$.pipe(
    switchMap(() => this.getJokes()),
    pluck<JokeResponse, 'value'>('value'),
    shareReplay(1)
  );

  constructor(
    private readonly httpClient: HttpClient,
    private readonly matSnackBar: MatSnackBar
  ) {}

  getJokes(amount: number = 10): Observable<JokeResponse> {
    return this.httpClient.get<JokeResponse>(`@api/jokes/random/${amount}`).pipe(
      catchError(() => {
        const matSnackBarRef = this.matSnackBar.open(`Something went wrong...`);
        of(void 0).pipe(delay(1000)).subscribe(() => matSnackBarRef?.dismiss());
        return of({ type: 'fail', value: [] } as JokeResponse);
      })
    );
  }

  triggerUpdate(): void {
    this.trigger$.next();
  }
}
