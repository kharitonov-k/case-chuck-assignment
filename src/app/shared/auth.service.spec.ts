import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TestScheduler } from 'rxjs/testing';

import { AuthService, authTokensStoreKey, authTokenStoreKey } from './auth.service';
import { LoginComponent } from './login/login.component';

describe('AuthService', () => {
  let service: AuthService;
  let matDialog: MatDialog;
  let scheduler: TestScheduler;

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => expect(actual).toEqual(expected));
    TestBed.configureTestingModule({
      providers: [
        { provide: MatDialog, useValue: { open() {return { dismiss() {} }} } }
      ]
    });
    service = TestBed.inject(AuthService);
    matDialog = TestBed.inject(MatDialog);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update token', () => {
    spyOn(localStorage, 'getItem').withArgs(authTokensStoreKey).and.returnValue(`[]`);
    const spyOnSetItem = spyOn(localStorage, 'setItem');
    scheduler.run(({ expectObservable }) => {
      const token = '123';
      service.updateToken(token);
      expectObservable(service.token$).toBe('(ab)', { a: null, b: token });
      expect(spyOnSetItem).toHaveBeenCalledWith(authTokensStoreKey, JSON.stringify([ token ]));
    });
  });

  it('should login', () => {
    scheduler.run(({ expectObservable, cold }) => {
      spyOn(matDialog, 'open').and.returnValue({
        afterClosed() { return cold('a', { a: true }); }
      } as unknown as MatDialogRef<LoginComponent>);
      expectObservable(service.login()).toBe('(a|)', { a: true });
    });
  });
});
