import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';

import { of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { AuthService, authTokenStoreKey } from './auth.service';
import { JokesFavoriteService, jokesStoreKey } from './jokes-favorite.service';
import { Joke } from './jokes.types';
import { switchMap, tap } from 'rxjs/operators';

describe('JokesFavoriteService', () => {
  let service: JokesFavoriteService;
  let authService: AuthService;
  let matSnackBar: MatSnackBar;
  let scheduler: TestScheduler

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => expect(actual).toEqual(expected));
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatSnackBar,
          useValue: { open() {return { dismiss() {} }} }
        },
        {
          provide: AuthService,
          useValue: {
            // @ts-ignore
            get isAuthorized$() {return of();},
            // @ts-ignore
            get token$() {return of();},
            login() {return of();}
          }
        },
      ]
    });
    authService = TestBed.inject(AuthService);
    matSnackBar = TestBed.inject(MatSnackBar);
  });

  it('should be created', () => {
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    service = TestBed.inject(JokesFavoriteService);
    expect(service).toBeTruthy();
  });

  it('should add joke', () => {
    spyOn(localStorage, 'getItem')
      .withArgs(authTokenStoreKey).and.returnValue(`xxx`)
      .withArgs(jokesStoreKey).and.returnValue(`{"xxx":{}}`);
    scheduler.run(({ expectObservable, cold }) => {
      const joke = { id: 1 } as Joke;
      spyOnProperty(authService, 'isAuthorized$')
        .and.returnValue(cold('a', { a: true }));
      spyOnProperty(authService, 'token$')
        .and.returnValue(cold('a', { a: 'xxx' }));
      spyOn(authService, 'login')
        .and.returnValue(cold('a', { a: true }));
      service = TestBed.inject(JokesFavoriteService);
      const trigger$ = cold('----a').pipe(
        tap(() => service.add(joke))
      );
      expectObservable(service.jokes$).toBe('a---b', { a: [], b: [ joke ] });
      trigger$.subscribe();
    });
  });

  it('should remove joke', () => {
    spyOn(localStorage, 'getItem')
      .withArgs(authTokenStoreKey).and.returnValue(`xxx`)
      .withArgs(jokesStoreKey).and.returnValue(`{"xxx":{"1":{"id":1}}}`);
    const spyOnSetItem = spyOn(localStorage, 'setItem').and.stub();
    scheduler.run(({ expectObservable, cold }) => {
      const joke = { id: 1 } as Joke;
      spyOnProperty(authService, 'isAuthorized$')
        .and.returnValue(cold('a', { a: true }));
      spyOnProperty(authService, 'token$')
        .and.returnValue(cold('a', { a: 'xxx' }));
      spyOn(authService, 'login')
        .and.returnValue(cold('a', { a: true }));
      service = TestBed.inject(JokesFavoriteService);
      const trigger$ = cold('300ms a').pipe(
        tap(() => service.remove(joke))
      );
      expectObservable(service.jokes$).toBe('a 299ms b', { a: [ joke ], b: [] });
      trigger$.subscribe();
    });
  });

  it('should toggle joke', () => {
    spyOn(localStorage, 'getItem')
      .withArgs(authTokenStoreKey).and.returnValue(`xxx`)
      .withArgs(jokesStoreKey).and.returnValue(`{"xxx":{"1":{"id":1}}}`);
    const spyOnSetItem = spyOn(localStorage, 'setItem').and.stub();
    scheduler.run(({ expectObservable, cold }) => {
      const joke = { id: 1 } as Joke;
      spyOnProperty(authService, 'isAuthorized$')
        .and.returnValue(cold('a', { a: true }));
      spyOnProperty(authService, 'token$')
        .and.returnValue(cold('a', { a: 'xxx' }));
      spyOn(authService, 'login')
        .and.returnValue(cold('a', { a: true }));
      service = TestBed.inject(JokesFavoriteService);
      const trigger$ = cold('300ms a').pipe(
        tap(() => service.toggle(joke)),
        switchMap(() => cold('300ms a')),
        tap(() => service.toggle(joke))
      );
      expectObservable(service.jokes$).toBe('a 299ms b 299ms c', { a: [ joke ], b: [], c: [ joke ] });
      trigger$.subscribe();
    });
  });

  it('should check is favorite', () => {
    spyOn(localStorage, 'getItem')
      .withArgs(authTokenStoreKey).and.returnValue(`xxx`)
      .withArgs(jokesStoreKey).and.returnValue(`{"xxx":{"1":{"id":1}}}`);
    const spyOnSetItem = spyOn(localStorage, 'setItem').and.stub();
    scheduler.run(({ expectObservable, cold }) => {
      const joke = { id: 1 } as Joke;
      spyOnProperty(authService, 'isAuthorized$')
        .and.returnValue(cold('a', { a: true }));
      spyOnProperty(authService, 'token$')
        .and.returnValue(cold('a', { a: 'xxx' }));
      spyOn(authService, 'login')
        .and.returnValue(cold('a', { a: true }));
      service = TestBed.inject(JokesFavoriteService);
      const trigger$ = cold('300ms a').pipe(
        tap(() => service.toggle(joke)),
        switchMap(() => cold('300ms a')),
        tap(() => service.toggle(joke))
      );
      expectObservable(service.isFavorite(joke)).toBe('a 299ms b 299ms c', { a: true, b: false, c: true });
      trigger$.subscribe();
    });
  });

  it('should empty favorites', () => {
    spyOn(localStorage, 'getItem')
      .withArgs(authTokenStoreKey).and.returnValue(`xxx`)
      .withArgs(jokesStoreKey).and.returnValue(`{"xxx":{"1":{"id":1},"2":{"id":2}}}`);
    const spyOnSetItem = spyOn(localStorage, 'setItem').and.stub();
    scheduler.run(({ expectObservable, cold }) => {
      spyOnProperty(authService, 'isAuthorized$')
        .and.returnValue(cold('a', { a: true }));
      spyOnProperty(authService, 'token$')
        .and.returnValue(cold('a', { a: 'xxx' }));
      spyOn(authService, 'login')
        .and.returnValue(cold('a', { a: true }));
      service = TestBed.inject(JokesFavoriteService);
      const trigger$ = cold('300ms a').pipe(
        tap(() => service.empty())
      );
      expectObservable(service.jokes$).toBe('a 299ms b', { a: [ { id: 1 }, { id: 2 } ], b: [] });
      trigger$.subscribe();
    });
  });
});
