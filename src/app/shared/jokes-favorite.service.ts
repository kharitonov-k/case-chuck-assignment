import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { MatSnackBar } from '@angular/material/snack-bar';

import { BehaviorSubject, iif, Observable, of } from 'rxjs';
import { filter, map, shareReplay, switchMap, tap, take, delay } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Joke, JokesFavorite, JokesFavoriteDB } from './jokes.types';
import { AuthService, getToken } from './auth.service';

export const jokesStoreKey = 'jokes-favorite';
const getFavoritesDB = (): JokesFavoriteDB => {
  try {
    const db = localStorage.getItem(jokesStoreKey);
    return db ? JSON.parse(db) : {};
  } catch (e) {
    return {};
  }
}
const getFavorites = (): JokesFavorite => {
  const token = getToken();
  const db = getFavoritesDB();
  return token ? (db[ token ] ?? {}) : {};
}
const setFavorites = (token: string, jokes: JokesFavorite) => {
  const db = getFavoritesDB();
  try {
    localStorage.setItem(jokesStoreKey, JSON.stringify({ ...db, [ token ]: jokes }));
  } catch (e) {
  }
}

@Injectable({
  providedIn: 'root'
})
export class JokesFavoriteService {

  private readonly store$ = new BehaviorSubject(isPlatformBrowser(this.platformId) ? getFavorites() : {});
  private readonly token$ = this.authService.isAuthorized$.pipe(
    switchMap((isAuthorized: boolean) => iif(
      () => isAuthorized,
      this.authService.token$,
      this.authService.login().pipe(switchMap(() => this.authService.token$))
    ).pipe(
      filter(token => typeof token === 'string'),
      map(String),
      take(1),
    ))
  );
  private readonly jokesStore$ = this.token$.pipe(
    tap(() => {
      this.store$.observers.forEach(o => o.complete());
      this.store$.next(getFavorites())
    }),
    switchMap((token: string) => this.store$.pipe(
      tap((jokes: JokesFavorite) => setFavorites(token, jokes)),
    )),
    shareReplay(1)
  );
  readonly jokes$: Observable<Joke[]> = this.jokesStore$.pipe(
    map((jokes: JokesFavorite) => Object.values(jokes)),
    shareReplay(1)
  );

  constructor(
    @Inject(PLATFORM_ID) private readonly platformId: string,
    private readonly matSnackBar: MatSnackBar,
    private readonly authService: AuthService,
  ) {}

  add(joke: Joke): void {
    this.checkAuthorization(() => {
      const value = this.store$.getValue();
      if (value[ joke.id ]) {
        return;
      }
      const { maxFavorites } = environment;
      if (Object.keys(value).length === maxFavorites) {
        const matSnackBarRef = this.matSnackBar.open(`You can add maximum ${maxFavorites} jokes to the favorites`);
        of(void 0).pipe(delay(1000)).subscribe(() => matSnackBarRef?.dismiss());
        return;
      }
      this.store$.next({ ...value, [ joke.id ]: joke });
    });
  }

  remove(joke: Joke): void {
    this.checkAuthorization(() => {
      const value = this.store$.getValue();
      if (!value[ joke.id ]) {
        return;
      }
      const { [ joke.id ]: target, ...rest } = value;
      this.store$.next(rest);
    });
  }

  toggle(joke: Joke): void {
    const value = this.store$.getValue();
    if (value[ joke.id ]) {
      return this.remove(joke);
    }
    return this.add(joke);
  }

  isFavorite(joke: Joke): Observable<boolean> {
    return this.authService.isAuthorized$.pipe(
      switchMap(isAuthorized => iif(
        () => isAuthorized,
        this.jokesStore$.pipe(
          map((jokes: JokesFavorite) => jokes.hasOwnProperty(joke.id))
        ),
        of(false)
      ))
    );
  }

  empty(): void {
    this.checkAuthorization(() => {
      this.store$.next({});
    });
  }

  private checkAuthorization(callback: () => void): void {
    this.authService.isAuthorized$.pipe(
      take(1),
      switchMap(isAuthorized => iif(
        () => isAuthorized,
        of(isAuthorized),
        this.authService.login()
      ))
    ).subscribe(isAuthorized => {
      if (isAuthorized) {
        callback();
      }
    });
  }
}
