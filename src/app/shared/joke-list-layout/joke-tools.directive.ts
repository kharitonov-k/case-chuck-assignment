import { Directive, HostBinding } from '@angular/core';

@Directive({ selector: '[appJokeTools]' })
export class JokeToolsDirective {
  @HostBinding('class.app-joke-tools')
  readonly rootClass: boolean = true;
}
