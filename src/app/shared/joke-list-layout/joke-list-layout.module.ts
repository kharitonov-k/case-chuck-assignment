import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JokeListLayoutDirective } from './joke-list-layout.directive';
import { JokeListDirective } from './joke-list.directive';
import { JokeToolsDirective } from './joke-tools.directive';
import { JokeListEmptyDirective } from './joke-list-empty.directive';


@NgModule({
  declarations: [
    JokeListLayoutDirective,
    JokeListDirective,
    JokeToolsDirective,
    JokeListEmptyDirective
  ],
  exports: [
    JokeListLayoutDirective,
    JokeListDirective,
    JokeToolsDirective,
    JokeListEmptyDirective
  ],
  imports: [
    CommonModule
  ]
})
export class JokeListLayoutModule {}
