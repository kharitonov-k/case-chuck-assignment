import { Directive, HostBinding } from '@angular/core';

@Directive({ selector: '[appJokeListLayout]' })
export class JokeListLayoutDirective {
  @HostBinding('class.app-joke-list-layout')
  readonly rootClass: boolean = true;
}
