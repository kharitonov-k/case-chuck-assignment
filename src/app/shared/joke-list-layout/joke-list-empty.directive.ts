import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appJokeListEmpty]'
})
export class JokeListEmptyDirective {
  @HostBinding('class.app-joke-empty')
  readonly rootClass: boolean = true;
}
