import { Directive, HostBinding } from '@angular/core';

@Directive({ selector: '[appJokeList]' })
export class JokeListDirective {
  @HostBinding('class.app-joke-list')
  readonly rootClass: boolean = true;
}
