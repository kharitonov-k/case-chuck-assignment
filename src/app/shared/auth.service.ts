import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { MatDialog } from '@angular/material/dialog';

import { concat, Observable, of, ReplaySubject } from 'rxjs';
import { filter, map, shareReplay, switchMap, take } from 'rxjs/operators';

import { LoginComponent } from './login/login.component';


export const authTokenStoreKey = 'jokes-auth-token';
export const getToken = (): string | null => localStorage.getItem(authTokenStoreKey);
const setToken = (token: string | null): void => {
  if (token) {
    localStorage.setItem(authTokenStoreKey, token);
    return;
  }
  localStorage.removeItem(authTokenStoreKey);
}
export const authTokensStoreKey = 'jokes-auth-tokens';
export const getTokens = (): string[] => {
  try {
    const tokens = localStorage.getItem(authTokensStoreKey);
    return tokens ? JSON.parse(tokens) : [];
  } catch (e) {
    return [];
  }
}
const setTokens = (tokens: string[]) => {
  try {
    localStorage.setItem(authTokensStoreKey, JSON.stringify(tokens));
  } catch (e) {
  }
}
export const calcToken = async (...parts: string[]): Promise<string> => {
  const data = new TextEncoder().encode(`${parts.join(':')}`);
  // works only for https of localhost
  const hashBuffer: ArrayBuffer = await crypto.subtle.digest('SHA-256', data);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  return hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly tokenUpdate$ = new ReplaySubject<string | null>(1);
  readonly token$: Observable<string | null> = (
    isPlatformBrowser(this.platformId)
      ? concat(of(getToken()), this.tokenUpdate$)
      : of(null)
  ).pipe(shareReplay(1));
  readonly isAuthorized$: Observable<boolean> = this.token$.pipe(map(Boolean));

  constructor(
    @Inject(PLATFORM_ID) private readonly platformId: string,
    private readonly matDialog: MatDialog
  ) {}

  updateToken(token: string | null): void {
    setToken(token);
    const tokens = getTokens();
    if (token && !tokens.includes(token)) {
      setTokens([ ...tokens, token ]);
    }
    this.tokenUpdate$.next(token);
  }

  login(): Observable<boolean> {
    return of(void 0).pipe(
      switchMap(() => {
        const dialogRef = this.matDialog.open<LoginComponent, undefined, boolean>(LoginComponent, {
          autoFocus: true,
          closeOnNavigation: true,
          disableClose: true
        });
        return dialogRef.afterClosed().pipe(
          filter(isAuthorized => isAuthorized !== undefined),
          map(Boolean),
          take(1)
        );
      })
    );

  }
}
