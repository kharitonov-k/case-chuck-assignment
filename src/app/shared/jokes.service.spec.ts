import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';

import { tap } from 'rxjs/operators';
import { TestScheduler } from 'rxjs/testing';

import { JokesService } from './jokes.service';

describe('JokesService', () => {
  let service: JokesService;
  let httpClient: HttpClient;
  let matSnackBar: MatSnackBar;
  let scheduler: TestScheduler;

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => expect(actual).toEqual(expected));
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: { get() {return of();} } },
        { provide: MatSnackBar, useValue: { open() {return { dismiss() {} }} } }
      ]
    });
    httpClient = TestBed.inject(HttpClient);
    matSnackBar = TestBed.inject(MatSnackBar);
    service = TestBed.inject(JokesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get jokes', () => {
    scheduler.run(({ expectObservable, cold }) => {
      spyOn(httpClient, 'get').and.returnValue(cold('a', { a: { type: 'success', value: [] } }));
      const trigger$ = cold('a').pipe(
        tap(() => service.triggerUpdate())
      );
      expectObservable(service.jokes$).toBe('a', { a: [] });
      trigger$.subscribe();
    });
  });

  it('should show error', () => {
    scheduler.run(({ expectObservable, cold }) => {
      spyOn(httpClient, 'get').and.returnValue(cold('#', { a: null }));
      const spyOnOpen = spyOn(matSnackBar, 'open').and.stub();
      const trigger$ = cold('a').pipe(
        tap(() => service.triggerUpdate())
      );
      expectObservable(service.jokes$.pipe(
        tap(() => {
          expect(spyOnOpen).toHaveBeenCalledWith(`Something went wrong...`);
        })
      )).toBe('a', { a: [] });
      trigger$.subscribe();
    });
  });
});

function of() {
  throw new Error('Function not implemented.');
}

