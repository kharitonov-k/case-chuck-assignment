import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

export class ApiInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const match = req.url.match(/^@api(.*)/);
    if (match) {
      const [ , url ] = match;
      return next.handle(req.clone({ url: `${environment.api}${url}` }));
    }
    return next.handle(req);
  }
}
