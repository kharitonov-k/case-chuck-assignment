import { SecurityContext } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';

import { JokesComponent } from './jokes.component';
import { JokesService } from '../shared/jokes.service';
import { JokesFavoriteService } from '../shared/jokes-favorite.service';
import { Joke } from '../shared/jokes.types';

describe('JokesComponent', () => {
  let component: JokesComponent;
  let fixture: ComponentFixture<JokesComponent>;
  let jokesService: JokesService;
  let jokesFavoriteService: JokesFavoriteService;
  let domSanitizer: DomSanitizer;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        { provide: JokesService, useValue: { triggerUpdate() {} } },
        { provide: JokesFavoriteService, useValue: { toggle() {}, isFavorite() {} } },
        { provide: DomSanitizer, useValue: { sanitize() {} } },
      ],
      declarations: [ JokesComponent ]
    }).compileComponents();
  });

  beforeEach(() => {
    jokesService = TestBed.inject(JokesService);
    jokesFavoriteService = TestBed.inject(JokesFavoriteService);
    domSanitizer = TestBed.inject(DomSanitizer);
    fixture = TestBed.createComponent(JokesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test triggerUpdate', () => {
    const spyOnTriggerUpdate = spyOn(jokesService, 'triggerUpdate').and.callThrough();
    component.triggerUpdate();
    expect(spyOnTriggerUpdate).toHaveBeenCalled();
  });

  it('should test toggleFavorite', () => {
    const joke = {} as Joke;
    const spyOnToggle = spyOn(jokesFavoriteService, 'toggle').and.callThrough();
    component.toggleFavorite(joke);
    expect(spyOnToggle).toHaveBeenCalledWith(joke);
  });

  it('should test isFavoriteJoke', () => {
    const joke = {} as Joke;
    const spyOnToggle = spyOn(jokesFavoriteService, 'isFavorite').and.callThrough();
    component.isFavoriteJoke(joke);
    expect(spyOnToggle).toHaveBeenCalledWith(joke);
  });

  it('should test trackById', () => {
    const joke = { id: 123 } as Joke;
    expect(component.trackById(0, joke)).toBe(123);
  });

  it('should test sanitizeHtml', () => {
    const content = `<div>123</div>`;
    const spyOnSanitize = spyOn(domSanitizer, 'sanitize').and.callThrough();
    component.sanitizeHtml(content)
    expect(spyOnSanitize).toHaveBeenCalledWith(SecurityContext.HTML, content);
  });
});
