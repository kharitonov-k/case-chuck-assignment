import { Component, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Observable } from 'rxjs';

import { Joke } from '../shared/jokes.types';
import { JokesService } from '../shared/jokes.service';
import { JokesFavoriteService } from '../shared/jokes-favorite.service';
import { JokeListLayoutDirective } from '../shared/joke-list-layout/joke-list-layout.directive';


@Component({
  selector: 'app-jokes',
  templateUrl: './jokes.component.html',
  styleUrls: [ './jokes.component.scss' ]
})
export class JokesComponent extends JokeListLayoutDirective {

  readonly jokes$: Observable<Joke[]> = this.jokesService.jokes$;

  constructor(
    private readonly jokesService: JokesService,
    private readonly jokesFavoriteService: JokesFavoriteService,
    private readonly domSanitizer: DomSanitizer
  ) {
    super();
  }

  triggerUpdate(): void {
    this.jokesService.triggerUpdate();
  }

  toggleFavorite(joke: Joke): void {
    this.jokesFavoriteService.toggle(joke);
  }

  isFavoriteJoke(joke: Joke): Observable<boolean> {
    return this.jokesFavoriteService.isFavorite(joke);
  }

  trackById(index: number, { id }: Joke): number {
    return id;
  }

  sanitizeHtml(content: string): string | null {
    return this.domSanitizer.sanitize(SecurityContext.HTML, content);
  }

}
