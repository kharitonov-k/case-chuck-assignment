import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { AppComponent } from './app.component';
import { AuthService } from './shared/auth.service';
import { JokesFavoriteService } from './shared/jokes-favorite.service';

describe('AppComponent', () => {
  let authService: AuthService;
  let jokesFavoriteService: JokesFavoriteService;
  let scheduler: TestScheduler;

  beforeEach(async () => {
    scheduler = new TestScheduler((actual, expected) => expect(actual).toEqual(expected));
    await TestBed.configureTestingModule({
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      imports: [
        RouterTestingModule
      ],
      providers: [
        {
          provide: AuthService,
          useValue: {
            // @ts-ignore
            get isAuthorized$() {},
            // @ts-ignore
            updateToken() {}
          }
        },
        {
          provide: JokesFavoriteService, useValue: {
            // @ts-ignore
            get jokes$() {}
          }
        }
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    authService = TestBed.inject(AuthService);
    jokesFavoriteService = TestBed.inject(JokesFavoriteService);
  });

  it('should create the app', () => {
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should provide the favorite joke count', () => {
    scheduler.run(({ expectObservable, cold }) => {
      spyOnProperty(authService, 'isAuthorized$').and.returnValue(cold('a', { a: true }));
      spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(cold('a-b-c', { a: [], b: [ 1 ], c: [ 1, 2, 3 ] }));
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      expectObservable(app.jokeCount$).toBe('a-b-c', { a: 0, b: 1, c: 3 });
    });
  });

  it('should update token with null', () => {
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    const spyOnLogout = spyOn(authService, 'updateToken');
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.logout();
    expect(spyOnLogout).toHaveBeenCalledWith(null);
  });
});
