import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JokesComponent } from './jokes/jokes.component';
import { JokesFavoriteGuard } from './jokes-favorite/jokes-favorite.guard';

const routes: Routes = [
  { path: '', component: JokesComponent },
  {
    path: 'favorites',
    loadChildren: () => import('./jokes-favorite/jokes-favorite.module').then(m => m.JokesFavoriteModule),
    canActivate: [ JokesFavoriteGuard ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { initialNavigation: 'enabled' }) ],
  exports: [ RouterModule ],
  providers: [ JokesFavoriteGuard ]
})
export class AppRoutingModule {}
