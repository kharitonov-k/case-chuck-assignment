import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JokesFavoriteModule } from './jokes-favorite/jokes-favorite.module';
import { SharedModule } from './shared/shared.module';
import { JokesComponent } from './jokes/jokes.component';
import { JokeListLayoutModule } from './shared/joke-list-layout/joke-list-layout.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';

@NgModule({
  declarations: [
    AppComponent,
    JokesComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule.forRoot(),
    MatTabsModule,
    MatIconModule,
    JokeListLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
