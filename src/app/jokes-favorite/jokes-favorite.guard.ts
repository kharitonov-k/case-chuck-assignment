import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { iif, Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

import { AuthService } from '../shared/auth.service';

@Injectable()
export class JokesFavoriteGuard implements CanActivate {
  constructor(
    @Inject(PLATFORM_ID) private readonly platformId: string,
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return isPlatformBrowser(this.platformId) ? this.authService.isAuthorized$.pipe(
      switchMap((isAuthorized: boolean) => iif(
        () => isAuthorized,
        of(isAuthorized),
        this.authService.login()
      ).pipe(
        map((isLoggedIn: boolean) => isLoggedIn || this.router.parseUrl('/'))
      )),
    ) : true;
  }
}
