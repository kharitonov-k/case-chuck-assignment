import { CUSTOM_ELEMENTS_SCHEMA, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { TestScheduler } from 'rxjs/testing';

import { AuthService } from '../shared/auth.service';
import { JokesFavoriteService } from '../shared/jokes-favorite.service';
import { JokesService } from '../shared/jokes.service';
import { JokesFavoriteComponent } from './jokes-favorite.component';
import { Joke } from '../shared/jokes.types';


describe('JokesFavoriteComponent', () => {
  let component: JokesFavoriteComponent;
  let fixture: ComponentFixture<JokesFavoriteComponent>;
  let authService: AuthService;
  let jokesFavoriteService: JokesFavoriteService;
  let jokesService: JokesService;
  let domSanitizer: DomSanitizer;
  let scheduler: TestScheduler;

  beforeEach(async () => {
    scheduler = new TestScheduler((actual, expected) => expect(actual).toEqual(expected));
    await TestBed.configureTestingModule({
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      imports: [
        RouterTestingModule
      ],
      providers: [
        {
          provide: AuthService,
          useValue: {
            // @ts-ignore
            get isAuthorized$() {return of();}
          }
        },
        {
          provide: JokesFavoriteService,
          useValue: {
            // @ts-ignore
            get jokes$() {return of();},
            add() {},
            toggle() {},
            isFavorite() {},
            empty() {}
          }
        },
        { provide: JokesService, useValue: { getJokes() {return of();} } },
        { provide: DomSanitizer, useValue: { sanitize() {} } }
      ],
      declarations: [ JokesFavoriteComponent ]
    }).compileComponents();
    authService = TestBed.inject(AuthService);
    jokesService = TestBed.inject(JokesService);
    jokesFavoriteService = TestBed.inject(JokesFavoriteService);
    domSanitizer = TestBed.inject(DomSanitizer);
  });

  it('should create', () => {
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    fixture = TestBed.createComponent(JokesFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should test startFilling', () => {
    scheduler.run(({ expectObservable, cold, hot }) => {
      spyOnProperty(authService, 'isAuthorized$')
        .and.returnValue(cold('a', { a: true }));
      spyOn(jokesService, 'getJokes').withArgs(1)
        .and.returnValue(hot('(a|)', { a: { type: 'success', value: [ { id: 1, categories: [], joke: '' } ] } }));
      spyOnProperty(jokesFavoriteService, 'jokes$')
        .and.returnValue(cold('a 5000ms b 5000ms c', {
        a: [ {}, {}, {}, {}, {}, {}, {}, {} ],
        b: [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
        c: [ {}, {}, {}, {}, {}, {}, {}, {}, {}, {} ],
      }));
      spyOn(jokesFavoriteService, 'add');
      fixture = TestBed.createComponent(JokesFavoriteComponent);
      component = fixture.componentInstance;
      expectObservable(component.isFilling$).toBe('a 10001ms b', { a: true, b: false });
      component.startFilling();
    });
  });

  it('should test stopFilling', () => {
    scheduler.run(({ expectObservable, cold }) => {
      spyOnProperty(authService, 'isAuthorized$').and.returnValue(cold('a', { a: true }));
      spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(cold('a', { a: [] }));
      fixture = TestBed.createComponent(JokesFavoriteComponent);
      component = fixture.componentInstance;
      expectObservable(component.isFilling$).toBe('a', { a: false });
      component.stopFilling();
    });
  });

  it('should test toggleFilling', () => {
    scheduler.run(({ expectObservable, cold }) => {
      spyOnProperty(authService, 'isAuthorized$').and.returnValue(cold('a', { a: true }));
      spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(cold('a', { a: [] }));
      fixture = TestBed.createComponent(JokesFavoriteComponent);
      component = fixture.componentInstance;
      const stream$ = cold('a').pipe(
        tap(() => component.toggleFilling()),
        switchMap(() => cold('5000ms a')),
        tap(() => component.toggleFilling())
      );
      expectObservable(component.isFilling$).toBe('(a b) 4996ms c', { a: false, b: true, c: false });
      stream$.subscribe();
    });
  });

  it('should test toggleFavorite', () => {
    const joke = {} as Joke;
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    const spyOnToggle = spyOn(jokesFavoriteService, 'toggle').and.stub();
    fixture = TestBed.createComponent(JokesFavoriteComponent);
    component = fixture.componentInstance;
    component.toggleFavorite(joke);
    expect(spyOnToggle).toHaveBeenCalledWith(joke);
  });

  it('should test isFavoriteJoke', () => {
    const joke = {} as Joke;
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    const spyOnIsFavorite = spyOn(jokesFavoriteService, 'isFavorite').and.stub();
    fixture = TestBed.createComponent(JokesFavoriteComponent);
    component = fixture.componentInstance;
    component.isFavoriteJoke(joke);
    expect(spyOnIsFavorite).toHaveBeenCalledWith(joke);
  });

  it('should test trackById', () => {
    const joke = { id: 1 } as Joke;
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    fixture = TestBed.createComponent(JokesFavoriteComponent);
    component = fixture.componentInstance;
    expect(component.trackById(0, joke)).toBe(1);
  });

  it('should test sanitizeHtml', () => {
    const content = `<a>123</a>`;
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    const spyOnSanitize = spyOn(domSanitizer, 'sanitize').and.stub();
    fixture = TestBed.createComponent(JokesFavoriteComponent);
    component = fixture.componentInstance;
    component.sanitizeHtml(content);
    expect(spyOnSanitize).toHaveBeenCalledWith(SecurityContext.HTML, content);
  });

  it('should test isFavoriteJoke', () => {
    spyOnProperty(authService, 'isAuthorized$').and.returnValue(of(true));
    spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(of([]));
    const spyOnEmpty = spyOn(jokesFavoriteService, 'empty').and.stub();
    fixture = TestBed.createComponent(JokesFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.empty();
    expect(spyOnEmpty).toHaveBeenCalledWith();
  });

  it('should test clean', () => {
    scheduler.run(({ expectObservable, cold }) => {
      spyOnProperty(authService, 'isAuthorized$').and.returnValue(cold('a', { a: true }));
      spyOnProperty(jokesFavoriteService, 'jokes$').and.returnValue(cold('a', { a: [] }));
      fixture = TestBed.createComponent(JokesFavoriteComponent);
      component = fixture.componentInstance;
      const stream$ = cold('a').pipe(
        switchMap(() => cold('5000ms a')),
        tap(() => component.clean()),
      );
      expectObservable(component.jokes$).toBe('(a b) 4996ms c', { a: [], b: [], c: [] });
      stream$.subscribe();
    });
  });
});
