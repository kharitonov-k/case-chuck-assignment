import { Component, Inject, OnDestroy, PLATFORM_ID, SecurityContext } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { BehaviorSubject, combineLatest, concat, forkJoin, merge, Observable, of, Subject } from 'rxjs';
import { delay, filter, map, shareReplay, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Joke } from '../shared/jokes.types';
import { JokesFavoriteService } from '../shared/jokes-favorite.service';
import { JokeListLayoutDirective } from '../shared/joke-list-layout/joke-list-layout.directive';
import { JokesService } from '../shared/jokes.service';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-jokes-favorite',
  templateUrl: './jokes-favorite.component.html',
  styleUrls: [ './jokes-favorite.component.scss' ]
})
export class JokesFavoriteComponent extends JokeListLayoutDirective implements OnDestroy {

  private readonly favoriteJokes$ = isPlatformBrowser(this.platformId) ? this.jokesFavoriteService.jokes$ : of([]);
  private readonly triggerJokes$ = new Subject<void>();
  readonly jokes$: Observable<Joke[]> = concat(
    of(void 0),
    merge(
      this.triggerJokes$,
      this.authService.isAuthorized$.pipe(
        tap(isAuthorized => {
          if (isAuthorized || !isPlatformBrowser(this.platformId)) {
            return;
          }
          this.router.navigate([ '/' ]);
        })
      )
    )
  ).pipe(
    switchMap(() => this.favoriteJokes$.pipe(take(1))),
    shareReplay(1)
  );
  readonly jokeCount$: Observable<number> = this.favoriteJokes$.pipe(
    map(jokes => jokes.length)
  );
  readonly isDirty$: Observable<boolean> = combineLatest(
    [ this.jokeCount$, this.jokes$.pipe(map(jokes => jokes.length)) ]
  ).pipe(map(([ jokeCount, viewCount ]) => jokeCount !== viewCount))
  readonly isFull$: Observable<boolean> = this.jokeCount$.pipe(
    map(jokeCount => jokeCount === environment.maxFavorites)
  );
  readonly hasJokes$: Observable<boolean> = this.jokeCount$.pipe(
    map(jokeCount => jokeCount !== 0)
  );
  readonly isFilling$ = new BehaviorSubject<boolean>(false);

  private readonly destroy$ = new Subject<void>();

  constructor(
    @Inject(PLATFORM_ID) private readonly platformId: string,
    private readonly authService: AuthService,
    private readonly jokesService: JokesService,
    private readonly jokesFavoriteService: JokesFavoriteService,
    private readonly domSanitizer: DomSanitizer,
    private readonly router: Router
  ) {
    super();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  toggleFavorite(joke: Joke): void {
    this.jokesFavoriteService.toggle(joke);
  }

  isFavoriteJoke(joke: Joke): Observable<boolean> {
    return this.jokesFavoriteService.isFavorite(joke);
  }

  trackById(index: number, { id }: Joke): number {
    return id;
  }

  sanitizeHtml(content: string): string | null {
    return this.domSanitizer.sanitize(SecurityContext.HTML, content);
  }

  empty(): void {
    this.jokesFavoriteService.empty();
  }

  clean(): void {
    this.triggerJokes$.next();
  }

  startFilling(): void {
    this.isFilling$.next(true);
    this.processFilling().subscribe();
  }

  stopFilling(): void {
    this.isFilling$.next(false);
  }

  toggleFilling(): void {
    this.isFilling$.pipe(take(1)).subscribe(isFilling => {
      if (isFilling) {
        return this.stopFilling();
      }
      this.startFilling();
    });
  }

  private processFilling(): Observable<Joke> {
    const triggerFilling$ = new Subject<void>();
    return concat(of(void 0), triggerFilling$).pipe(
      switchMap(() => forkJoin([
        this.jokesService.getJokes(1).pipe(
          filter(({ type, value: [ joke ] }) => type === 'success' && !!joke)
        ),
        of(void 0).pipe(delay(5000)),
      ])),
      map(([ { value: [ joke ] } ]) => joke),
      tap(joke => {
        this.jokesFavoriteService.add(joke);
        this.triggerJokes$.next();
        triggerFilling$.next();
      }),
      takeUntil(
        merge(
          this.destroy$,
          this.isFull$.pipe(
            filter(isFull => isFull),
            tap(() => this.isFilling$.next(false))
          ),
          this.isFilling$.pipe(filter(isFilling => !isFilling))
        ).pipe(take(1))
      )
    )
  }

}
