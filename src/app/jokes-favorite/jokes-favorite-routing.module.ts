import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JokesFavoriteComponent } from './jokes-favorite.component';

const routes: Routes = [
  { path: '', component: JokesFavoriteComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class JokesFavoriteRoutingModule {}
