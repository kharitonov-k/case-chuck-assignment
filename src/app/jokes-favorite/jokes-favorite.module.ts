import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { JokesFavoriteRoutingModule } from './jokes-favorite-routing.module';
import { JokesFavoriteComponent } from './jokes-favorite.component';
import { JokeListLayoutModule } from '../shared/joke-list-layout/joke-list-layout.module';


@NgModule({
  declarations: [
    JokesFavoriteComponent
  ],
  imports: [
    CommonModule,
    JokesFavoriteRoutingModule,
    MatCardModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    JokeListLayoutModule,
    MatSlideToggleModule,
    MatProgressBarModule
  ]
})
export class JokesFavoriteModule {}
