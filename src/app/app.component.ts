import { Component } from '@angular/core';

import { iif, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { JokesFavoriteService } from './shared/jokes-favorite.service';
import { AuthService } from './shared/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
  readonly isAuthorized$ = this.authService.isAuthorized$;
  readonly jokeCount$: Observable<number> = this.isAuthorized$.pipe(
    switchMap(isAuthorized => iif(
      () => isAuthorized,
      this.jokesFavoriteService.jokes$.pipe(map(jokes => jokes.length)),
      of(0)
    ))
  );

  constructor(
    private readonly authService: AuthService,
    private readonly jokesFavoriteService: JokesFavoriteService
  ) {}

  logout(): void {
    this.authService.updateToken(null);
  }
}
